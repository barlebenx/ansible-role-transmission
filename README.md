# Ansible Role: Transmission

[![pipeline status](https://gitlab.com/barlebenx/ansible-role-transmission/badges/master/pipeline.svg)](https://gitlab.com/barlebenx/ansible-role-transmission/-/commits/master)

Install transmission daemon.

## Requirements

- cf ``meta/main.yml`` for OS version requirements

## Role Variables

Available variables are listed on `defaults/main.yml`

## Dependencies

None

## Example playbook

```yaml
- hosts: all
  roles:
    - role: transmission
```
